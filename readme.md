### 当前项目状态
- [&check;] 文字打印
- [&check;] 列表表格打印
- [&check;] 行内左右分开对齐打印
- [&check;] 二维码打印（传递参数为二维码数据即可）[2023.2.13记：安卓打印时，二维码错误！！后续得空再修复！！]

### 使用方法
1. 请将此项目克隆到您的小程序应用根目录的 BLETicketPrinter 文件夹
```
git clone https://gitee.com/in-wechat/BLETicketPrinter.git BLETicketPrinter
```
2. 编辑 app.json，增加一个打印需要调用的页面
```
{
    "pages":[
        "BLETicketPrinter/pages/printer/printer"
    ]
}
```
3. 调用代码 /pages/index/index.js
``` 
import { Printer } from "../../BLETicketPrinter/printer";

Page({
    data: {
        order: {
            //.....
        }
    },
    onLoad: function (options) {
        this.initData();
    },
    //过程可以选择从服务器获取数据
    initData(){
        this.setData({
            order: {
                //订单数据
                order_id: "202207120513",
                state_string: "已完成",
                make_time: "2022年7月12日",
                time: "2022-07-12 13:14:20",
                //开单员
                employee_name: "幺月儿",
                employee_phone: "186xxxx8888",
                //客户信息
                customer_name: "张三",
                customer_phone: "138xxxx8998",
                //店铺信息
                shop_name: "广场中心印刷店",
                shop_address: "青秀区民族大道xxx号新纪广场",
                shop_phone: "159xxxx0246",
                //开单商品列表
                sku_list: [
                    {name: "冰糖黄皮果苗", quantity: 50, price: 12, amount: 10},
                    {name: "贵妃黄皮果苗", quantity: 20, price: 18, amount: 15},
                    {name: "黑金刚黄皮果苗", quantity: 30, price: 13, amount: 10},
                    {name: "用于种植时顺便施肥的肥料", quantity: 20, price: 90, amount: 80}
                ],
                //订单金额
                sku_amount: 3150,
                amount: 2500,
                sku_discount: 450,
                discount: 200,
                fee: 0,
                //商品数量
                sku_quantity: 120
            }
        });
    },
    //打印，注意，我本次打印所使用的设备打印的小票宽度是32个字符，其它机器，请另行调整
    print_ing: false,
    print(){
        if(this.print_ing) return;
        this.print_ing = true;
        let order = this.data.order;
        //将订单的商品转换成一个列表，用于打印出一个带表头的表格
        let sku_table = [];
        //表头，一维数组，多少个值代表多少列，这里使用3列
        sku_table.push(["名称", "数量", "单价"]);
        //表格每一行放一个商品
        for(let i=0; i<order.sku_list.length; i++){
            let sku = order.sku_list[i];
            //数字需要黑的为字符串，以免打印时有意外错误
            sku_table.push([sku.name, sku.quantity.toString(), sku.amount.toFixed(2)]);
        }
        //
        let printer = new Printer("gb2312");
        printer.init()
            //纸张宽度，默认为32个字符，打印表格的时候，需要根本纸张宽度设置每列的宽度
            .setPaperWidth(32)
            //开始打印建议条一行空白
            //先打印店铺名称作为开头
            .newLine().printText(order.shop_name)
            //使用一行等于号来填充，作为分割线
            .newLine().fillText("=")
            //订单号，注意为了格式，1个空格=1个字符。1个汉字=2个字符
            .newLine().printText("订 单 号：" + order.order_id.toString())
            .newLine().printText("订单状态：" + order.state_string)
            .newLine().printText("开 单 员：" + order.employee_name + "(" + order.employee_phone.toString() + ")")
            .newLine().printText("客    户：" + order.customer_name + "(" + order.customer_phone.toString() + ")")
            .newLine().printText("开单时间：" + order.make_time)
            //整页纸是32个字符宽度，打印一个表格，3列，第1列16个字符宽度，第2列4个字符宽度，第3列8个字符宽度
            //由于加了边框，宽度即：1+16+1+4+1+8+1，预留了边框的字符
            .newLine().printTable(sku_table, [16,4,8], true)
            //打印左右对齐的2段文字，1段居左，1段居右
            .newLine().printLRText("商品总数量：", order.sku_quantity.toString() + "件")
            .newLine()
            .newLine().printLRText("商品总金额：", order.sku_amount.toFixed(2))
            .newLine().printLRText("其它费用：", "+" + order.fee.toFixed(2))
            .newLine().printLRText("客户专属优惠：", "-" + order.sku_discount.toFixed(2))
            .newLine().printLRText("店铺额外优惠：", "-" + order.discount.toFixed(2))
            .newLine()
            .newLine().printLRText("实收金额：", order.amount.toFixed(2))
            .newLine()
            .newLine().printText("店铺地址：" + order.shop_address)
            .newLine().printText("联系电话：" + order.shop_phone)
            .newLine().fillText("-")
            //二维码
            .newLine().printQRCode("https://gitee.com/in-wechat/BLETicketPrinter-simple")
            //打印一行专用文字，32个字符宽度
            .newLine().printText("=======幺月儿的小票打印机=======")
            //最后，为一能够让最后一行打印出来，并且保证切割，请打印2~3行空白
            .newLine().newLine().newLine();
        //
        wx.navigateTo({
            //链接可以添加一些参数：
            // auto_print=true   表示，使用历史设置，自动连接，并自动打印
            // device_name=xxxx  表示限定只能连接对应xxxx名称的蓝牙设备
            url: "/BLETicketPrinter/pages/printer/printer?auto_print=true",
            events: {
                printSuccess: () => {
                    //打印完成会通知该方法，由于是跨页面，不可使用弹窗提醒等方法！
                }
            },
            success: res => {
                res.eventChannel.emit("setPrinter", printer);
            }
        });
        this.print_ing = false;
    }
});
```

![alt 语法示例](https://www.inphp.cc/assets/images/BLEPrinter.jpg)

请参考示例项目：https://gitee.com/in-wechat/BLETicketPrinter-simple